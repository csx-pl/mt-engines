<?php
/**
 * MMT Engine wrapper, performs API calls.
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Engine;

class Mmt implements EngineInterface
{
	/**
	 * Engine config
	 * @var array
	 */
	private $config = [];

	/**
	 * Language map MemoQ to MMT
	 * @var array
	 */ 
	private $mapLangCodes = [
		'pol' => 'pl',
		'eng' => 'en',
	];

	/**
	 * Unicode normalizer
	 * @var \Normalizer
	 */ 
	private $normalizer;

	/* ====================================================================== */

	/**
	 * Create new engine, check required parameters
	 *
	 * @param array $config
	 */
	public function __construct($config = [])
	{
		if (empty($config['location']))
			throw new \Exception("Brak parametru 'location'");

		$this->normalizer = new \Normalizer();
		$this->config = $config;
	}

	/* ====================================================================== */
	
	/**
	 * Return engine name
	 * 
	 * @return string
	 */ 
	public function getName()
	{
		return (new \ReflectionClass($this))->getShortName();
	}

	/* ====================================================================== */
	
	/**
	 * Translate single source text
	 *
	 * @param  string $source
	 * @param  string $srcLang source language code
	 * @param  string $trgLang target language code
	 * @return string
	 */
	public function translate($source = '', $srcLang = '', $trgLang = '')
	{
		$source_lang = (!empty($srcLang)) ? $srcLang : self::DEFAULT_SRC_LANG;
		$target_lang = (!empty($trgLang)) ? $trgLang : self::DEFAULT_TRG_LANG;

		// check for lang mapping
		if (isset($this->mapLangCodes[ $source_lang ]))
			$source_lang = $this->mapLangCodes[ $source_lang ];
		if (isset($this->mapLangCodes[ $target_lang ]))
			$target_lang = $this->mapLangCodes[ $target_lang ];

		$query = [
			'source' => $source_lang,
			'target' => $target_lang,
			'q' => $this->normalizeUnicode($source),
		];

		if (isset($this->config['query']['translate']))
			$query = array_merge($query, $this->config['query']['translate']);

		$url = $this->config['location'].'/translate/?'.http_build_query($query);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		if ($out['status'] != 200)
		{
			$_msg = 'Nieznany błąd';
			
			if (isset($out['error']['type']))
				$_msg = $out['error']['type'];
			if (isset($out['error']['message']))
				$_msg .= ': '.$out['error']['message'];

			throw new \Exception($_msg);
		}

		return $out['data']['translation'];
	}

	/* ====================================================================== */
	
	/**
	 * Teach engine new source/target text
	 *
	 * @param  string $source
	 * @param  string $target
	 * @param  string $srcLang 3-letters source language code from MemoQ
	 * @param  string $trgLang 3-letters target language code from MemoQ
	 * @return string learn job id
	 */
	public function learn($source = '', $target = '', $srcLang = '', $trgLang = '')
	{
		$source_lang = (!empty($srcLang)) ? $srcLang : self::DEFAULT_SRC_LANG;
		$target_lang = (!empty($trgLang)) ? $trgLang : self::DEFAULT_TRG_LANG;

		// check for lang mapping
		if (isset($this->mapLangCodes[ $source_lang ]))
			$source_lang = $this->mapLangCodes[ $source_lang ];
		if (isset($this->mapLangCodes[ $target_lang ]))
			$target_lang = $this->mapLangCodes[ $target_lang ];

		$query = http_build_query([
			'source' => $source_lang,
			'target' => $target_lang,
			'sentence' => $this->normalizeUnicode($source),
			'translation' => $this->normalizeUnicode($target),
		]);

		if (empty($this->config['learn_domain_id']))
			throw new \Exception("Brak parametru 'learn_domain_id'");

		$url = $this->config['location'].'/memories/'.$this->config['learn_domain_id'].'/corpus';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);
		
		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		if ($out['status'] != 200)
			throw new \Exception($out['error']['message']);

		return $out['data']['id'];
	}

	/* ====================================================================== */
	
	/**
	 * Get engine info, status
	 *
	 * @return string|bool RAW json encoded
	 */
	public function getInfo()
	{
		$url = $this->config['location'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$parsed = json_decode((string)$out, true);

		if ($parsed['status'] != 200)
			throw new \Exception($parsed['error']['message']);

		return $out;
	}

	/* ====================================================================== */
	
	/**
	 * Get nice parsed engine info, status
	 * 
	 * @return string
	 */ 
	public function engineInfo()
	{
		$info = $this->getInfo();
		$info = json_decode((string)$info, true);
		$result = 'Engine status: '.$info['status']." ";
		if ($info['status'] == 200)
			$result .= "(OK)";
		$result .= ", version: ".$info['data']['build']['version'].", build: ".$info['data']['build']['number']."\r\n";

		$i = 0;
		foreach ($info['data']['cluster']['nodes'] as $node)
		{
			$lang = '';
			foreach ($node['languages'] as $l)
			{
				if (isset($l['source']) && isset($l['target']))
					$lang .= $l['source']." -> ".$l['target']." ";
				elseif (is_array($l))
					$lang .= $l[0]." -> ".$l[1]." ";
			}
			$result .= "Node $i (addr: ".$node['address'].") status: ".$node['status'].", ".$lang."\r\n";

			$i++;
		}

		return $result;
	}

	/* ====================================================================== */
	
	/**
	 * Import new TMX file to engine
	 * 
	 * @param string $filePath
	 * @return string
	 */
	public function importTmx($filePath = '')
	{
		if (empty($this->config['learn_domain_id']))
			throw new \Exception("Brak parametru 'learn_domain_id'");
		if (empty($this->config['tmx_import_path']))
			throw new \Exception("Brak parametru 'tmx_import_path'");

		$query = http_build_query([
			'content_type' => 'TMX',
			'local_file' => $this->config['tmx_import_path'].'/'.$filePath,
		]);

		$url = $this->config['location'].'/memories/'.$this->config['learn_domain_id'].'/corpus';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		if ($out['status'] != 200)
			throw new \Exception($out['error']['message']);
		
		// @todo parse response

		return $out;
	}

	/* ====================================================================== */
	
	/**
	 * Get progress of import job
	 * 
	 * @param string $jobId
	 * @return string
	 */ 
	public function getImportProgress($jobId = '')
	{
		$url = $this->config['location'].'/memories/imports/'.$jobId;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPGET, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		if ($out['status'] != 200)
			throw new \Exception($out['error']['message']);

		// @todo parse response

		return $out;
	}

	/* ====================================================================== */

	/**
	 * Normalize UTF-8 string. Change composite characters to normal (ex. o + ` => ó)
	 * 
	 * @param string $str
	 * @return string
	 */ 
	public function normalizeUnicode($str)
	{
		// replace hardspace (&nbsp;) with normal space		
		$str = str_replace(' ', ' ', $str);
		$str = str_replace('\xc2\xa0', ' ', $str);

		if (!$this->normalizer->isNormalized($str))
			$str = $this->normalizer->normalize($str);

		return $str;
	}
}
