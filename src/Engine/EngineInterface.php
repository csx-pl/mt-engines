<?php
/**
 * MT Engine interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Engine;

interface EngineInterface
{
	const DEFAULT_SRC_LANG = 'pol';
	const DEFAULT_TRG_LANG = 'eng';

	public function translate($source = '', $srcLang = '', $trgLang = '');
	public function engineInfo();
	public function getName();
}