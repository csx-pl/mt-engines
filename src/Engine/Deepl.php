<?php
/**
 * DeepL Engine wrapper, performs API calls.
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Engine;

class Deepl implements EngineInterface
{
	/**
	 * DeepL API endpoint
	 * @var string
	 */ 
	const URL = "https://api.deepl.com/v2/";

	/**
	 * Engine config
	 * @var array
	 */
	private $config = [];

	/**
	 * Map 3-chars language codes to 2-chars
	 * @var array
	 */ 
	private $mapLangCodes = [
		'pol' => 'pl',
		'eng' => 'en',
		'ger' => 'de',
		'fre' => 'fr',
		'ita' => 'it',
		'spa' => 'es',
	];

	/* ====================================================================== */
	
	/**
	 * Create new engine, check required parameters
	 *
	 * @param array $config
	 */
	public function __construct($config = [])
	{
		if (empty($config['auth_key']))
			throw new \Exception("Brak parametru 'auth_key'");

		$this->config = $config;
	}

	/* ====================================================================== */
	
	/**
	 * Return engine name
	 * 
	 * @return string
	 */ 
	public function getName()
	{
		return (new \ReflectionClass($this))->getShortName();
	}

	/* ====================================================================== */
	
	/**
	 * Translate single source text
	 *
	 * @param  string $text
	 * @param  string $srcLang source language code
	 * @param  string $trgLang target language code
	 * @return string
	 */
	public function translate($text = '', $srcLang = '', $trgLang = '')
	{
		// inline tags merged together as single string 
		// to be appended at the end of translation in case input has mismatched tags 
		$tagsMerged = '';

		$source_lang = (!empty($srcLang)) ? $srcLang : self::DEFAULT_SRC_LANG;
		$target_lang = (!empty($trgLang)) ? $trgLang : self::DEFAULT_TRG_LANG;

		// check for lang mapping
		if (isset($this->mapLangCodes[ $source_lang ]))
			$source_lang = $this->mapLangCodes[ $source_lang ];
		if (isset($this->mapLangCodes[ $target_lang ]))
			$target_lang = $this->mapLangCodes[ $target_lang ];

		// check if opening tags match closing ones
		$this->validateInlineTags($text, $tagsMerged);

		$post = [
			'auth_key' => $this->config['auth_key'],
			'source_lang' => strtoupper($source_lang),
			'target_lang' => strtoupper($target_lang),
			'tag_handling' => 'xml',
			'text' => $text,
		];

		// add formality parameter when translating into polish
		if ($target_lang === 'pl')
			$post['formality'] = 'more';

		$post = http_build_query($post, '', '&', PHP_QUERY_RFC3986);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::URL.'translate');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);
		
		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		// check response header
		$curlInfo = curl_getinfo($ch);
		if ($curlInfo['http_code'] !== 200)
			throw new \Exception($out['message']);

		return $out['translations'][0]['text'].$tagsMerged;
	}

	/* ====================================================================== */
	
	/**
	 * Get engine info, status
	 *
	 * @return string|bool RAW json encoded
	 */
	public function getInfo()
	{
		$post = [
			'auth_key' => $this->config['auth_key']
		];
		$post = http_build_query($post, '', '&', PHP_QUERY_RFC3986);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::URL.'usage?'.$post);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		return $out;
	}

	/* ====================================================================== */
	
	/**
	 * Get nice parsed engine info, chars count/limit
	 * 
	 * @return string
	 */ 
	public function engineInfo()
	{
		$info = $this->getInfo();		
		$info = json_decode((string)$info, true);

		$out = sprintf("Character count: %s\r\nCharacter limit: %s", 
			$info['character_count'], 
			$info['character_limit']);

		return $out;
	}

	/* ====================================================================== */
	
	/**
	 * Check if number of open inline tags matches closing tags.
	 * If not then merge tags as string to $tagsMerged and strip them from input text
	 * 
	 * @param string $text
	 * @param string $tagsMerged
	 */ 
	private function validateInlineTags(&$text, &$tagsMerged)
	{
		$needFix = false;
		preg_match_all('#<inline_tag id="\d{1,3}">#', $text, $tagsOpen);
		preg_match_all('#</inline_tag>#', $text, $tagsClose);

		// check for open/close count match
		// if (count($tagsOpen[0]) !== count($tagsClose[0]))
		// 	$needFix = true;

		// check if open tag is before close
		$tags = [];
		$offset = 0;
		foreach ($tagsOpen[0] as $tag)
		{
			$poz = strpos($text, $tag, $offset);
			$tags[$poz] = 1;
			$offset = $poz+1;
		}

		$offset = 0;
		foreach ($tagsClose[0] as $tag)
		{
			$poz = strpos($text, $tag, $offset);
			$tags[$poz] = -1;
			$offset = $poz+1;
		}

		// sort by position in text
		ksort($tags);

		$score = 0;
		foreach ($tags as $tag)
		{
			$score += $tag;
			if ($score < 0)
			{
				$needFix = true;
				break;
			}
		}

		if ($needFix)
		{
			// $tagsMerged = implode($tagsOpen[0]).implode($tagsClose[0]);
			$text = strip_tags($text);
		}
	}
}