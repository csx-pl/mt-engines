<?php
/**
 * Google translate wrapper, performs API calls.
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Engine;

class Google implements EngineInterface
{
	/**
	 * Google API endpoint
	 * @var string
	 */ 
	const URL = "https://translation.googleapis.com/language/translate/v2";

	/**
	 * Engine config
	 * @var array
	 */
	private $config = [];

	/**
	 * Map 3-chars language codes to 2-chars
	 * @var array
	 */ 
	private $mapLangCodes = [
		'pol' => 'PL',
		'eng' => 'EN',
	];

	/* ====================================================================== */
	
	/**
	 * Create new engine, check required parameters
	 *
	 * @param array $config
	 */
	public function __construct($config = [])
	{
		$configDefault = [
			'format' => 'text',
		];

		if (empty($config['auth_key']))
			throw new \Exception("Brak parametru 'auth_key'");

		$this->config = array_merge($configDefault, $config);
	}

	/* ====================================================================== */
	
	/**
	 * Return engine name
	 * 
	 * @return string
	 */ 
	public function getName()
	{
		return (new \ReflectionClass($this))->getShortName();
	}

	/* ====================================================================== */
	
	/**
	 * Translate single source text
	 *
	 * @param  string $text
	 * @param  string $srcLang source language code
	 * @param  string $trgLang target language code
	 * @return string
	 */
	public function translate($text = '', $srcLang = '', $trgLang = '')
	{
		$source_lang = (!empty($srcLang)) ? $srcLang : self::DEFAULT_SRC_LANG;
		$target_lang = (!empty($trgLang)) ? $trgLang : self::DEFAULT_TRG_LANG;

		// check for lang mapping
		if (isset($this->mapLangCodes[ $source_lang ]))
			$source_lang = $this->mapLangCodes[ $source_lang ];
		if (isset($this->mapLangCodes[ $target_lang ]))
			$target_lang = $this->mapLangCodes[ $target_lang ];

		$post = [
			'key' => $this->config['auth_key'],
			'source' => $source_lang,
			'target' => $target_lang,
			'q' => $text,
		];
		$post = http_build_query($post, '', '&', PHP_QUERY_RFC3986);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::URL);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (isset($this->config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $this->config['proxy']);

		$out = curl_exec($ch);

		// check for error
		if ($out === false)
			throw new \Exception(curl_error($ch));

		$out = json_decode((string)$out, true);

		if (!empty($out['error']['message']))
			throw new \Exception($out['error']['message']);

		if (empty($out['data']['translations'][0]['translatedText']))
			throw new \Exception("Brak przetłumaczonego tekstu");
		$translated = $out['data']['translations'][0]['translatedText'];

		return $translated;
	}

	/* ====================================================================== */
	
	/**
	 * Get nice parsed engine info
	 * 
	 * @return string
	 */ 
	public function engineInfo()
	{
		return "The Google Cloud Translation API";
	}
}