<?php
/**
 * MT cache interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Cache;

interface CacheInterface
{
	public function get($key = '');
	public function getMeta($key = '');
	public function set($key = '', $source = '', $target = '');
	public function update($source = '', $target = '');
	public function setSrcLangCode($srcLangCode);
	public function setTrgLangCode($trgLangCode);
	public function setUser($user);
}