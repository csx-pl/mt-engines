<?php
/**
 * MMT cache - dummy engine
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Cache;

class Dummy implements CacheInterface
{
	/**
	 * Check if given key is present in cache storage and return it
	 * 
	 * @param string $key
	 * @return void
	 */ 
	public function get($key = '')
	{
	}

	/* ====================================================================== */
	
	/**
	 * Store new entry in cache storage
	 * 
	 * @param string $key
	 * @param string $source
	 * @param string $target
	 * @return void
	 */ 
	public function set($key = '', $source = '', $target = '')
	{
	}

	/* ====================================================================== */
	
	/**
	 * Update existing cache entries with verified target content
	 * Not implemented.
	 * 
	 * @param string $source
	 * @param string $target
	 * @return void
	 */ 
	public function update($source = '', $target = '')
	{
	}

	/* ====================================================================== */
	
	/**
	 * @param string $key
	 * @return void
	 */ 
	public function getMeta($key = '')
	{
	}

	/* ====================================================================== */
	
	/**
	 * @param string $srcLangCode
	 * @return void
	 */
	public function setSrcLangCode($srcLangCode)
	{
	}

	/* ====================================================================== */
	
	/**
	 * @param string $trgLangCode
	 * @return void
	 */
	public function setTrgLangCode($trgLangCode)
	{
	}

	/* ====================================================================== */
	
	/**
	 * @param Object $user
	 * @return void
	 */
	public function setUser($user)
	{
	}
}