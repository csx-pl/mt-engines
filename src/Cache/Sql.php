<?php
/**
 * MMT cache - MySQL engine
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Cache;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Sql implements CacheInterface
{
	/**
	 * Name of the SQL table for storage
	 * @var string
	 */ 
	const TABLE = 'mmt_engine_cache';

	/**
	 * Name of the SQL users table
	 * @var string
	 */ 
	const TABLE_USERS = 'profiles';

	/**
	 * Time a cache entry is considered valid before expiring (used by strtotime)
	 * @var string
	 */ 
	private $cache_time = '+1 hour';

	/**
	 * PDO connection
	 * @var \PDO
	 */ 
	private $pdo;

	private $engineName = '';
	private $srcLangCode = '';
	private $trgLangCode = '';

	/**
	 * CSS user_id
	 * @var int
	 */ 
	private $user_id;

	/* ====================================================================== */
	
	/**
	 * Create engine, inject MySQL PDO connection
	 * 
	 * @param string $cache_time
	 */ 
	public function __construct($cache_time)
	{
		$this->cache_time = $cache_time;
	}

	/* ====================================================================== */
	
	/**
	 * Inject PDO object for storage
	 * 
	 * @param \PDO $pdo
	 * @return void
	 */
	public function setPdo(\PDO $pdo)
	{
		$this->pdo = $pdo;	
	}

	/* ====================================================================== */
	
	/**
	 * Set engine name
	 * 
	 * @param string $engineName
	 * @return void
	 */
	public function setEngineName($engineName)
	{
		$this->engineName = $engineName;
	}

	/* ====================================================================== */
	
	/**
	 * Set source language code 
	 * 
	 * @param string $srcLangCode
	 * @return void
	 */
	public function setSrcLangCode($srcLangCode)
	{
		$this->srcLangCode = $srcLangCode;
	}

	/* ====================================================================== */
	
	/**
	 * Set target language code 
	 * 
	 * @param string $trgLangCode
	 * @return void
	 */
	public function setTrgLangCode($trgLangCode)
	{
		$this->trgLangCode = $trgLangCode;
	}

	/* ====================================================================== */
	
	/**
	 * Set CSS user info
	 * 
	 * @param Object $user
	 * @return void
	 */ 
	public function setUser($user)
	{
		$this->user_id = $user->user_id;
	}

	/* ====================================================================== */
	
	/**
	 * Check if given key is present in cache storage and return it
	 * 
	 * @param string $key
	 * @return string|bool
	 */ 
	public function get($key = '')
	{
		$stm = $this->pdo->prepare("SELECT `target` FROM `".self::TABLE."` 
			WHERE 
				`cacheKey` = ? AND 
				`expires` > ? 
			ORDER by `created` DESC LIMIT 1");
		$stm->execute([$key, date('Y-m-d H:i:s')]);
		$row = $stm->fetch(\PDO::FETCH_ASSOC);

		return isset($row['target']) ? $row['target'] : false;
	}

	/* ====================================================================== */
	
	/**
	 * Get cache meta information (dates: created, expires, reviewied; isReviewed, user firstName, lastName)
	 * 
	 * @param string $key
	 * @return array|bool
	 */ 
	public function getMeta($key = '')
	{
		$stm = $this->pdo->prepare("SELECT `c`.`created`, `c`.`expires`, `c`.`reviewed`, `c`.`isReviewed`, `p`.`firstName`, `p`.`lastName` 
			FROM `".self::TABLE."` AS `c`
			LEFT JOIN `".self::TABLE_USERS."` AS `p` ON (`c`.`user_id` = `p`.`user_id`)
			WHERE 
				`cacheKey` = ? AND 
				`expires` > ? 
			ORDER by `created` DESC LIMIT 1");
		$stm->execute([$key, date('Y-m-d H:i:s')]);
		$row = $stm->fetch(\PDO::FETCH_ASSOC);

		return isset($row) ? $row : false;		
	}

	/* ====================================================================== */
	
	/**
	 * Store new entry in cache storage
	 * 
	 * @param string $key
	 * @param string $source
	 * @param string $target
	 * @return bool
	 */ 
	public function set($key = '', $source = '', $target = '')
	{
		$stm = $this->pdo->prepare("INSERT INTO `".self::TABLE."` 
			(cacheKey, created, expires, engine, user_id, srcLangCode, trgLangCode, source, target) 
			VALUES (:cacheKey, :now, :expires, :engine, :user_id, :srcLangCode, :trgLangCode, :source, :target)");
		$stm->bindValue(':cacheKey', $key);
		$stm->bindValue(':now', date('Y-m-d H:i:s'));
		$stm->bindValue(':expires', date('Y-m-d H:i:s', strtotime($this->cache_time)));
		$stm->bindValue(':engine', $this->engineName);
		$stm->bindValue(':user_id', $this->user_id);
		$stm->bindValue(':srcLangCode', $this->srcLangCode);
		$stm->bindValue(':trgLangCode', $this->trgLangCode);
		$stm->bindValue(':source', $source);
		$stm->bindValue(':target', $target);
		$stm->execute();

		return true;
	}

	/* ====================================================================== */
	
	/**
	 * Update existing cache entries with verified target content
	 * 
	 * @param string $source
	 * @param string $target
	 * @return bool
	 */ 
	public function update($source = '', $target = '')
	{
		// skip if empty content
		if (empty($source) || empty($target))
			return false;

		// get matching cache hits
		$stm = $this->pdo->prepare("SELECT * FROM `".self::TABLE."` 
			WHERE `source` = :source AND `srcLangCode` = :srcLangCode AND `trgLangCode` = :trgLangCode");
		$stm->bindValue(':source', $source);
		$stm->bindValue(':srcLangCode', $this->srcLangCode);
		$stm->bindValue(':trgLangCode', $this->trgLangCode);
		$stm->execute();

		$stmUpdate = $this->pdo->prepare("UPDATE `".self::TABLE."` 
			SET `reviewed` = :now, `isReviewed` = 1, `user_id` = :user_id, `target` = :target 
			WHERE id = :id");
		$stmUpdate->bindValue(':target', $target);
		$stmUpdate->bindValue(':now', date('Y-m-d H:i:s'));
		$stmUpdate->bindValue(':user_id', $this->user_id);

		$rows = $stm->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$stmUpdate->bindValue(':id', $row['id']);
			$stmUpdate->execute();
		}

		return true;
	}
}