<?php
/**
 * MMT cache factory
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Cache;

use Psr\Log\LoggerInterface;

class CacheFactory
{
	/**
	 * Logger object
	 * @var LoggerInterface
	 */ 
	protected static $logger;

	/* ====================================================================== */
	
	/**
	 * Inject logger object
	 * 
	 * @param LoggerInterface $logger 
	 * @return void
	 */ 
	public static function setLogger(LoggerInterface $logger)
	{
		self::$logger = $logger;
	}

	/* ====================================================================== */
	
	/**
	 * Create cache object and inject needed objects/parameters
	 * 
	 * @param array $cacheConfig
	 * @param array $params
	 * @return Object
	 */ 
	public static function getInstance($cacheConfig, $params = [])
	{
		if (getenv('DEBUG'))
			self::$logger->debug('Creating cache object: '.$cacheConfig['className']);		

		// for php >= 5.6
		// $cache = new $cacheConfig['className'](...array_values($cacheConfig['construct']));

		// for php < 5.6
		$cacheClass = new \ReflectionClass($cacheConfig['className']);
		$cache = $cacheClass->newInstanceArgs(array_values($cacheConfig['construct']));

		foreach ($params as $paramName => $paramValue)
		{
			$methodName = 'set'.ucfirst($paramName);

			// inject object/parameters
			if (method_exists($cache, $methodName))
			{
				if (getenv('DEBUG'))
					self::$logger->debug('Injecting object/param via: '.$methodName, ['params' => $paramValue]);		

				call_user_func_array([$cache, $methodName], [$paramValue]);
			}
		}

		return $cache;
	}
}