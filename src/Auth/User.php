<?php
/**
 * CSS User interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Auth;

class User
{
	/**
	 * Login fail message
	 */
	const MSG_LOGIN_FAIL = 'Nieprawidłowy użytkownik';

	/**
	 * SQL connection
	 * @var \PDO
	 */
	private $pdo;

	/* ====================================================================== */
	
	/**
	 * Create new Auth object, set SQL connection
	 *
	 * @param \PDO $pdo
	 */
	public function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	/* ====================================================================== */
	
	/**
	 * Get CSS user or throw error if no user found or invalid credentials
	 *
	 * @param  string $username
	 * @param  string $pin
	 * @return array
	 */
	public function getUser($username = '', $pin = '')
	{
		$username = trim(filter_var($username, FILTER_SANITIZE_STRING));
		$pin = trim(filter_var($pin, FILTER_SANITIZE_STRING));

		// check login credentials
		$stm = $this->pdo->prepare("SELECT * FROM `users` 
			LEFT JOIN `profiles` ON (`users`.`id` = `profiles`.`user_id`) 
			WHERE `username` = :username LIMIT 1");
		$stm->bindValue(':username', $username);
		$stm->execute();

		$data = $stm->fetch(\PDO::FETCH_ASSOC);

		if (empty($data))
			throw new \Exception(self::MSG_LOGIN_FAIL);

		if ($data['active'] != 1)
			throw new \Exception(self::MSG_LOGIN_FAIL);

		if ($pin != $data['pin'])
			throw new \Exception(self::MSG_LOGIN_FAIL);

		return $data;
	}

	/* ====================================================================== */
	
	/**
	 * Get cached settings for user, or read settings from SQL
	 * 
	 * @param int $user_id
	 * @return array
	 */ 
	public function getSettings($user_id)
	{
		if (empty($user_id))
			throw new \Exception('Brak id użytkownika');

		// try to read from APC cache
		$settings = apcu_fetch('mt_user_'.$user_id);
		if ($settings === false)
		{
			// get user settings from SQL
			$stm = $this->pdo->prepare("SELECT * FROM `users`
				LEFT JOIN `profiles` ON (`users`.`id` = `profiles`.`user_id`) 
				WHERE `users`.`id` = :user_id LIMIT 1");
			$stm->bindValue(':user_id', $user_id);
			$stm->execute();

			$data = $stm->fetch(\PDO::FETCH_ASSOC);

			if (empty($data))
				throw new \Exception('Brak danych użytkownika');

			$settings = [
				'plugins' => [
					'mmt' => [
						'pl-en' => $data['mt_plugin_mmt_pl-en'],
						'en-pl' => $data['mt_plugin_mmt_en-pl'],
					],					
					'deepl' => [
						'pl-en' => $data['mt_plugin_deepl_pl-en'],
						'en-pl' => $data['mt_plugin_deepl_en-pl'],
						'de' => $data['mt_plugin_deepl_de'],
						'fr' => $data['mt_plugin_deepl_fr'],
						'it' => $data['mt_plugin_deepl_it'],
						'es' => $data['mt_plugin_deepl_es'],
					],					
				],
				'filters' => json_decode($data['mt_plugin_filters'], true),
			];

			// store for +30 days
			apcu_store('mt_user_'.$user_id, $settings, $ttl = 2592000);
		}

		return $settings;
	}

	/* ====================================================================== */
	
	/**
	 * Check if given user can use MT engine for translation
	 * 
	 * @param int $user_id
	 * @param string $engineName
	 * @param string $srcLangCode
	 * @param string $trgLangCode
	 * @return bool
	 */ 
	public function canUsePlugin($user_id, $engineName, $srcLangCode, $trgLangCode)
	{
		$engineName = strtolower($engineName);
		$settings = $this->getSettings($user_id);

		if ($srcLangCode == 'pol' && $trgLangCode == 'eng')
			$langPair = 'pl-en';
		elseif ($srcLangCode == 'eng' && $trgLangCode == 'pol')
			$langPair = 'en-pl';
		elseif ($srcLangCode == 'ger' || $trgLangCode == 'ger')
			$langPair = 'de';
		elseif ($srcLangCode == 'fre' || $trgLangCode == 'fre')
			$langPair = 'fr';
		elseif ($srcLangCode == 'ita' || $trgLangCode == 'ita')
			$langPair = 'it';
		elseif ($srcLangCode == 'spa' || $trgLangCode == 'spa')
			$langPair = 'es';
		else
			$langPair = $srcLangCode.'-'.$trgLangCode;

		if (isset($settings['plugins'][$engineName][$langPair]) && $settings['plugins'][$engineName][$langPair])
			return true;
		else
			return false;
	}
}
