<?php
/**
 * CSS User interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Auth;

interface AuthInterface
{
	public function setUser($user);
}