<?php
/**
 * MT Filter interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Filter;

interface FilterInterface
{
	public function filter($input = '', $srcLangCode = '', $trgLangCode = '', $type = '', &$rules = []);
}