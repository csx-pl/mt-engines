<?php
/**
 * Dummy filter, returns original input only
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Filter;

class Dummy implements FilterInterface
{
	
	/**
	 * Return input only
	 * 
	 * @param string $input
	 * @param string $srcLangCode
	 * @param string $trgLangCode
	 * @param string $type
	 * @param array $rules
	 * @return string
	 */ 
	public function filter($input = '', $srcLangCode = '', $trgLangCode = '', $type = '', &$rules = [])
	{
		return $input;
	}
}