<?php
/**
 * Memories filter
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Filter;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Memories implements FilterInterface
{
	/**
	 * Curl handler
	 * @var resource
	 */ 
	private $curl;

	/**
	 * App key for hash calculation
	 * @var string
	 */ 
	private $salt = '';

	/**
	 * CSS user_id
	 * @var int
	 */ 
	private $user_id;

	/* ====================================================================== */
	
	/**
	 * Create curl handler, set config options
	 * 
	 * @param array $config
	 */ 
	public function __construct($config = [])
	{
		if (empty($config['filterUrl']))
			throw new \Exception("Brak URL filtra");

		if (empty($config['app_salt']))
			throw new \Exception("Brak parametru app_salt");
		$this->salt = $config['app_salt'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $config['filterUrl']);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		if (!empty($config['proxy']))
			curl_setopt($ch, CURLOPT_PROXY, $config['proxy']);
		
		$this->curl = $ch;
	}

	/* ====================================================================== */
	
	/**
	 * Perform call to filtering app
	 * 
	 * @param string $input
	 * @param string $srcLangCode
	 * @param string $trgLangCode
	 * @param string $type [in|out]
	 * @param array $rules
	 * @return string
	 */ 
	public function filter($input = '', $srcLangCode = '', $trgLangCode = '', $type = '', &$rules = [])
	{
		// if nothing to filter, just return as it is
		if (empty($input))
			return $input;

		$data = [
			'type' => $type,
			'srcLangCode' => $srcLangCode,
			'trgLangCode' => $trgLangCode,
			'input' => $input,
			'user_id' => $this->user_id,
		];
		$hash = hash_hmac('sha256', serialize($data), $this->salt);
		$data['hash'] = $hash;

		curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));

		$res = curl_exec($this->curl);

		// check for error
		if ($res === false)
			throw new \Exception(curl_error($this->curl));

		$res = json_decode((string)$res, true);

		if ($res['status'] != 'ok')
			throw new \Exception($res['error']);

		if (isset($res['rules']))
			$rules = $res['rules'];

		return $res['output'];
	}

	/* ====================================================================== */
	
	/**
	 * Set CSS user info
	 * 
	 * @param Object $user
	 * @return void
	 */ 
	public function setUser($user)
	{
		$this->user_id = $user->user_id;
	}
}