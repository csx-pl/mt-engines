<?php
/**
 * MMT engine statistics storage - dummy class, discard all input
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Stats;

class Dummy implements StatsInterface
{
	/**
	 * @param string $engineName
	 * @param int $userId
	 * @param int $cntCharsSrc
	 * @param int $cntCharsTrg
	 * @return bool
	 */ 
	public function store($engineName, $userId, $cntCharsSrc = 0, $cntCharsTrg = 0)
	{
		return true;
	}
}