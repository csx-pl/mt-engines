<?php
/**
 * MT Stats interface
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Stats;

interface StatsInterface
{
	public function store($engineName, $userId, $cntCharsSrc = 0, $cntCharsTrg = 0);
}