<?php
/**
 * MMT engine statistics storage - CSV file storage
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Stats;

class Csv implements StatsInterface
{
	/**
	 * CSV file handler
	 * @var Resource
	 */ 
	private $csv;

	/* ====================================================================== */
	
	/**
	 * Create stats object
	 * 
	 * @param string $filePath
	 */ 
	public function __construct($filePath)
	{
		$this->csv = fopen($filePath, "a");
	}

	/* ====================================================================== */
	
	/**
	 * Close open file handler
	 */ 
	public function __destruct()
	{
		fclose($this->csv);
	}

	/* ====================================================================== */
	
	/**
	 * Store stats to CSV file with current date
	 * 
	 * @param string $engineName
	 * @param int $userId
	 * @param int $cntCharsSrc
	 * @param int $cntCharsTrg
	 * @return void
	 */ 
	public function store($engineName, $userId, $cntCharsSrc = 0, $cntCharsTrg = 0)
	{
		$date = date('Y-m-d H:i:s');
		$engineName = strtolower($engineName);
		
		fputcsv($this->csv, [$date, $engineName, $userId, $cntCharsSrc, $cntCharsTrg]);
	}
}