<?php
/**
 * MMT engine statistics storage - SQL storage
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

namespace ArteQ\CSX\MT\Stats;

class Sql implements StatsInterface
{
	/**
	 * Name of the SQL table for storage
	 * @var string
	 */ 
	const TABLE = 'mmt_engine_stats_daily_tmp';

	/**
	 * PDO connection
	 * @var \PDO
	 */ 
	private $pdo;

	/* ====================================================================== */
	
	/**
	 * Create engine, inject MySQL PDO connection
	 * 
	 * @param \PDO $pdo
	 */ 
	public function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	/* ====================================================================== */
	
	/**
	 * Store stats to SQL db with current date
	 * 
	 * @param string $engineName
	 * @param int $userId
	 * @param int $cntCharsSrc
	 * @param int $cntCharsTrg
	 * @return bool
	 */ 
	public function store($engineName, $userId, $cntCharsSrc = 0, $cntCharsTrg = 0)
	{
		$stm = $this->pdo->prepare('INSERT INTO `'.self::TABLE.'` (date, engine, user_id, cntCharsSrc, cntCharsTrg) 
			VALUES(?, ?, ?, ?, ? )');
		$stm->execute([date('Y-m-d H:i:s'), $engineName, $userId, $cntCharsSrc, $cntCharsTrg]);

		return true;
	}
}