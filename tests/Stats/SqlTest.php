<?php
/**
 * Test SQL stats
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\CSX\MT\Stats\Sql;

class SqlTest extends TestCase
{
	private $stats;
	private $pdo;

	/* ====================================================================== */
	
	/**
	 * Create Sqlite DB fixtures
	 */ 
	public function __construct()
	{
		$pdo = new \PDO('sqlite:'.__DIR__.'/stats.sqlite');
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// create cache fixtures
		$pdo->exec("CREATE TABLE IF NOT EXISTS `mmt_engine_stats_daily_tmp` (
		  `id` INTEGER PRIMARY KEY,
		  `date` datetime NOT NULL,
		  `engine` varchar(15) NOT NULL,
		  `user_id` int(11) NOT NULL,
		  `cntCharsSrc` int(11) NOT NULL,
		  `cntCharsTrg` int(11) NOT NULL
		);");

		$stats = new Sql($pdo);

		$this->stats = $stats;
		$this->pdo = $pdo;

		parent::__construct();
	}

	/* ====================================================================== */
	
	/**
	 * Remove Sqlite DB file
	 */ 
	public function __destruct()
	{
		@unlink(__DIR__.'/stats.sqlite');
	}

	/* ====================================================================== */
	
	public function testCanStore()
	{
		$this->assertTrue($this->stats->store($engine = 'Test Engine', $user_id = 1, $cntCharsSrc = 1000, $cntCharsTrg = 2000));
		$this->assertTrue($this->stats->store($engine = 'Test Engine', $user_id = 2, $cntCharsSrc = 111, $cntCharsTrg = 222));
	}

	/* ====================================================================== */
	
	public function testCanReceive()
	{
		$stm = $this->pdo->prepare('SELECT SUM(`cntCharsSrc`) AS `srcCnt`, SUM(`cntCharsTrg`) AS `trgCnt` FROM `mmt_engine_stats_daily_tmp`;');
		$stm->execute();
		$cnt = $stm->fetch(\PDO::FETCH_ASSOC);

		$this->assertEquals(1111, $cnt['srcCnt']);
		$this->assertEquals(2222, $cnt['trgCnt']);
	}
}