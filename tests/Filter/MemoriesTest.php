<?php
 /**
  * Test MT filters
  *
  * @author 	Artur Grącki <arteq@arteq.org>
  * @copyright 	Copyright (c) 2019. All rights reserved.
  */

 use PHPUnit\Framework\TestCase;
 use ArteQ\CSX\MT\Filter\Memories;

 class MemoriesTest extends TestCase
 {
 	private $filter;
 	private $pdo;

 	/* ====================================================================== */
 	
 	/**
 	 * Create Sqlite DB fixtures
 	 */ 
 	public function __construct()
 	{
 		$pdo = new \PDO('sqlite:'.__DIR__.'/filters.sqlite');
 		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 		// create cache fixtures
 		$pdo->exec("CREATE TABLE IF NOT EXISTS `mmt_filters` (
 		  `id` INTEGER PRIMARY KEY,
 		  `created` datetime NOT NULL,
 		  `created_by` int(11) NOT NULL,
 		  `modified` datetime NOT NULL,
 		  `modified_by` int(11) NOT NULL,
 		  `closed` tinyint(1) NOT NULL DEFAULT '0',
 		  `type` varchar(3) NOT NULL DEFAULT 'out',
 		  `isActive` tinyint(1) NOT NULL DEFAULT '1',
 		  `category` varchar(255) NOT NULL,
 		  `isRegExp` tinyint(1) NOT NULL DEFAULT '0',
 		  `isStrictCase` tinyint(1) NOT NULL DEFAULT '0',
 		  `srcLangCode` varchar(3) NOT NULL,
 		  `trgLangCode` varchar(3) NOT NULL,
 		  `source` varchar(255) NOT NULL,
 		  `target` varchar(255) NOT NULL
 		);");
 	
 		$this->pdo = $pdo;

 		parent::__construct();
	}

	/* ====================================================================== */
	
	/**
	 * Remove Sqlite DB file
	 */ 
	public function __destruct()
	{
		@unlink(__DIR__.'/filters.sqlite');
	}

	/* ====================================================================== */
	
	public function setUp()
	{
		$config = ['filterUrl' => 'http://localhost', 'app_salt' => 'abc123'];
		$filter = new Memories($config);
		$this->filter = $filter;
	}
	
	/* ====================================================================== */
	
	public function testFilterIn()
	{
		$this->markTestIncomplete('Not yet implemented');
	}

	public function testFilterOut()
	{
		$this->markTestIncomplete('Not yet implemented');
	}

	public function testFilterTag()
	{
		$this->markTestIncomplete('Not yet implemented');
	}
}