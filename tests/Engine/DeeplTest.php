<?php
/**
 * DeepL translate engine API test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2017. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\CSX\MT\Engine\Deepl;

class DeeplTest extends TestCase
{
	private $engine;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dotenv = new Dotenv\Dotenv(__DIR__.'/../../');
		$dotenv->load();
		
		$config = [
			'auth_key' => getenv('deepl_key'),
		];

		$this->engine = new Deepl($config);
	}

	/* ====================================================================== */
	
	public function testMissingConfig()
	{
		$this->expectException(\Exception::class);
		$engine = new Deepl();
	}

	/* ====================================================================== */
	
	public function testCanGetName()
	{
		$this->assertEquals('Deepl', $this->engine->getName());
	}

	/* ====================================================================== */
	
	public function testCanTranslatePolToEng()
	{
		$translated = $this->engine->translate('PHP jest super', 'pl', 'en');
		$this->assertEquals('PHP is great', $translated);

		$translated = $this->engine->translate('PHP jest super');
		$this->assertEquals('PHP is great', $translated);
	}

	/* ====================================================================== */
	
	public function testCanTranslateEngToPol()
	{
		$translated = $this->engine->translate('supervisory board', 'en', 'pl');
		$this->assertEquals('rada nadzorcza', $translated);
	}

	/* ====================================================================== */
	
	public function testCanTranslateGerToPol()
	{
		$translated = $this->engine->translate('Gute Planung positiv beeinflusst die Zukuft.', 'de', 'pl');
		$this->assertEquals('Dobre planowanie ma pozytywny wpływ na przyszłość.', $translated);
	}

	/* ====================================================================== */
	
	public function testCanTranslateXmlTags()
	{
		$translated = $this->engine->translate('<inline_tag id="0"><b>strona internetowa </b></inline_tag><inline_tag id="2"><b>Domu Maklerskiego</b> – oficjalna strona internetowa Domu Maklerskiego dostępna pod adresem: www.trigon.pl lub pod innym adresem wskazanym przez Dom Maklerski i podanym do wiadomości Klientów,</inline_tag>');
		$this->assertEquals('xxx', $translated);
	}
}