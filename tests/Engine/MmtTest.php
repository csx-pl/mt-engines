<?php
/**
 * MMT translate engine API test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2017. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\CSX\MT\Engine\Mmt;

class MmtTest extends TestCase
{
	private $engine;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dotenv = new Dotenv\Dotenv(__DIR__.'/../../');
		$dotenv->load();
		
		$config = [
			'location' => getenv('mmt_location_test'),
			'query' => [
				'translate' => [
					'context_vector' => '1:1.0',
					'verbose' => true,
				],
			],
		];

		$this->engine = new Mmt($config);
	}

	/* ====================================================================== */
	
	public function testMissingConfig()
	{
		$this->expectException(\Exception::class);
		$engine = new Mmt();
	}

	/* ====================================================================== */
	
	public function testCanGetName()
	{
		$this->assertEquals('Mmt', $this->engine->getName());
	}

	/* ====================================================================== */
	
	public function testCanTranslatePolToEng()
	{
		$translated = $this->engine->translate('rada nadzorcza', 'pl', 'en');
		$this->assertEquals('supervisory board', $translated);

		$translated = $this->engine->translate('rada nadzorcza', 'pol', 'eng');
		$this->assertEquals('supervisory board', $translated);

		$translated = $this->engine->translate('rada nadzorcza');
		$this->assertEquals('supervisory board', $translated);
	}

	/* ====================================================================== */
	
	public function testCanTranslateEngToPol()
	{
		$translated = $this->engine->translate('supervisory board', 'en', 'pl');
		$this->assertEquals('rada nadzorcza', $translated);

		$translated = $this->engine->translate('supervisory board', 'eng', 'pol');
		$this->assertEquals('rada nadzorcza', $translated);
	}

	/* ====================================================================== */
	
	public function testCantTranslateToUnsuportedLang()
	{
		$this->expectException(\Exception::class);
		$translated = $this->engine->translate('rada nadzorcza', 'pol', 'de');
	}

	/* ====================================================================== */

	public function testCanGetInfo()
	{
		$info = $this->engine->getInfo();
		$parsed = json_decode($info, true);

		$this->assertArrayHasKey('status', $parsed);
		$this->assertArrayHasKey('data', $parsed);
		$this->assertTrue($parsed['status'] == 200);
	}

	/* ====================================================================== */

	public function testCanNoralizeString()
	{
		// string contains \u{301} == \xCC\x81
		$malformed = 'Zamierzamy również sukcesywnie wymagać takich postaw od naszych dostawców surowców i półproduktów.';
		$normalized = $this->engine->normalizeUnicode($malformed);

		$this->assertEquals(1, preg_match('/\xCC\x81/', $malformed));
		$this->assertEquals(0, preg_match('/\xCC\x81/', $normalized));
	}	
}