<?php
/**
 * Google translate engine API test
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2017. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\CSX\MT\Engine\Google;

class GoogleTest extends TestCase
{
	private $engine;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dotenv = new Dotenv\Dotenv(__DIR__.'/../../');
		$dotenv->load();

		$config = [
			'auth_key' => getenv('google_key'),
		];

		$this->engine = new Google($config);
	}

	/* ====================================================================== */
	
	public function testMissingConfig()
	{
		$this->expectException(\Exception::class);
		$engine = new Google();
	}

	/* ====================================================================== */
	
	public function testCanGetName()
	{
		$this->assertEquals('Google', $this->engine->getName());
	}

	/* ====================================================================== */
	
	public function testCanTranslate()
	{
		$translated = $this->engine->translate('PHP jest super', 'PL', 'EN');
		$this->assertEquals('PHP is great', $translated);

		$translated = $this->engine->translate('PHP jest super');
		$this->assertEquals('PHP is great', $translated);
	}
}