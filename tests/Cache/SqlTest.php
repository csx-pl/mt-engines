<?php
/**
 * Test SQL cache
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2018. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\CSX\MT\Cache\Sql;

class SqlTest extends TestCase
{
	private $cache;
	private $pdo;

	/* ====================================================================== */
	
	/**
	 * Create Sqlite DB fixtures
	 */ 
	public function __construct()
	{
		$pdo = new \PDO('sqlite:'.__DIR__.'/cache.sqlite');
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// create cache fixtures
		$pdo->exec("CREATE TABLE IF NOT EXISTS `mmt_engine_cache` (
		  `id` INTEGER PRIMARY KEY,
		  `cacheKey` varchar(32) NOT NULL,
		  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `expires` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `reviewed` datetime DEFAULT NULL,
		  `engine` varchar(10) NOT NULL,
		  `user_id` int(11) NOT NULL,
		  `isReviewed` tinyint(1) DEFAULT '0',
		  `srcLangCode` varchar(3) NOT NULL,
		  `trgLangCode` varchar(3) NOT NULL,
		  `source` text NOT NULL,
		  `target` text NOT NULL
		);");

		// create user fixtures
		$pdo->exec("CREATE TABLE IF NOT EXISTS `profiles` (
		  `id` INTEGER PRIMARY KEY,
		  `user_id` INTEGER NOT NULL,
		  `firstName` varchar(50) NOT NULL,
		  `lastName` varchar(50) NOT NULL
		);");

		$pdo->exec("INSERT INTO `profiles` (user_id, firstName, lastName) 
			VALUES ('1', 'testowe imie', 'testowe nazwisko');");

		$cache = new Sql('+1 hour');
		$cache->setPdo($pdo);
		$cache->setEngineName('Test');
		$cache->setUser((object)['user_id' => 1]);
		$cache->setSrcLangCode('pol');
		$cache->setTrgLangCode('eng');

		$this->cache = $cache;
		$this->pdo = $pdo;

		parent::__construct();
	}

	/* ====================================================================== */
	
	/**
	 * Remove Sqlite DB file
	 */ 
	public function __destruct()
	{
		@unlink(__DIR__.'/cache.sqlite');
	}

	/* ====================================================================== */
	
	public function testCanSetEntry()
	{
		$this->assertTrue($this->cache->set('key_1', 'source text', 'target text'));
		$this->assertTrue($this->cache->set('key_2', 'source', 'target'));
	}

	/* ====================================================================== */
	
	public function testCanGetEntry()
	{
		$this->assertEquals('target text', $this->cache->get('key_1'));

		$meta = $this->cache->getMeta('key_1');
		$this->assertNull($meta['reviewed']);
		$this->assertEquals('0', $meta['isReviewed']);
		$this->assertEquals('testowe imie', $meta['firstName']);
	}

	/* ====================================================================== */
	
	public function testCantGetMissingEntry()
	{
		$this->assertFalse($this->cache->get('not_exist'));
		$this->assertFalse($this->cache->getMeta('not_exist'));
	}

	/* ====================================================================== */
	
	public function testCanUpdateEntry()
	{
		$this->assertTrue($this->cache->update('source', 'changed target'));
		$this->assertTrue($this->cache->update('not exist source', 'changed target'));
	}

	/* ====================================================================== */
	
	public function testCanGetUpdatedEntry()
	{
		$this->assertEquals('changed target', $this->cache->get('key_2'));

		$meta = $this->cache->getMeta('key_2');
		$this->assertNotNull($meta['reviewed']);
		$this->assertEquals('1', $meta['isReviewed']);
	}
}